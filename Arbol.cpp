// Se llaman las librerias a implementar
#include <iostream>
// Se llama al archivo cabecera del arbol
#include "Arbol.h"

using namespace std;
// Se llama al constructor
Arbol::Arbol(){

}
// NODO QUE SE ENCARGA DE AGREGARLO A LA CLASE ARBOL
Nodo* Arbol::agregar_nodo(int dato){
  Nodo *nuevoNodo;				//Crea un puntero del tipo NODO
  nuevoNodo = new Nodo;			//Se define una estructura del tipo nodo
  nuevoNodo->valor=dato;		//Se incorpora el valor
  nuevoNodo->izquierda=NULL;	//Se asigna NULL al puntero IZQUIERDA
  nuevoNodo->derecha=NULL;		//Se asigna NULL al puntero DERECHA
  return nuevoNodo;				//Se devuelve el puntero al nuevo nodo Creado
}

// ESTA FUNCION PERMITE QUE SE AGREGEN VALORES AL ARBOL POR EL NODO
void Arbol::agregar_valores(Nodo *&raiz, int dato){
	if(raiz == NULL){					//Raiz es el puntero del arbol creado, si es nulo se apunta al nuevo NODO
		cout << endl << "AGREGANDO VALORES AL NODO" << endl;
		Nodo *nuevoNodo;					//Puntero del tipo Nodo
		nuevoNodo = agregar_nodo(dato);
		raiz = nuevoNodo;
	}
	else{
		//Cuando la Raiz no es NULO se debe buscar la posicion del nuevo NODO recordando que menores a la izquierda mayores a la derecha

		if(dato < raiz->valor){		//Dato menor vamor a la izquierda
			agregar_valores(raiz->izquierda, dato); //Se llama a la misma funcion recursivamente hasta llegar a NULL
		}
		else{						//Caso contrario y el dato es mayor vamos a la derecha
			agregar_valores(raiz->derecha, dato);	//Se llama a la misma funcion recursivamente hasta llegar a NULL
		}
	}
}

// FUNCION QUE ELIMINA EL NODO
void Arbol::eliminar_nodo(Nodo* &raiz, int dato){

  if(raiz==NULL){			//Nada que eliminar y retorna
  	return;
  }
  else{ 					// Si no se cumple la primera condicion
  	if(raiz->valor==dato){		// A la raiz con el valor se le compara con el dato ingresado
  		if(raiz->izquierda==NULL){ // Si la raiz por la izquierda esta nula
  			raiz=raiz->derecha;   // Ira la raiz por la derecha
		  }	else{
		  	if(raiz->derecha==NULL){ // Si la raiz por la dercha esta nula
		  		raiz=raiz->izquierda; // Ira la raiz por la izquierda
			  }else{
			  	//Como ni Izquierda ni Derecha son NULL
			  	Nodo *reservaDerecha=raiz->derecha;		// Se crea un nodo de reserva derecha que se igual a la raiz por la derecha
			  	raiz=obtener_maximo(raiz->izquierda); // La raiz es igual a obtener el maximo por la izquierda
			  	raiz->derecha=reservaDerecha;
			  }
		  }
	  } else{
	  	if(raiz->valor > dato){		 // Si la raiz con el valor son mayores al dato
	  		Arbol raizIzquierda;	 // El arbol ira con la raiz por la izquierda
	  		raizIzquierda.raiz=raiz->izquierda;
	  		eliminar_nodo(raizIzquierda.raiz, dato); //Se elimina la raiz por la izquierda y el dato
	  		raiz->izquierda=raizIzquierda.raiz;		// La raiz queda actualizada.
		  } else{
		  	Arbol raizDerecha;					// El arbol ira con la raiz por la derecha
	  		raizDerecha.raiz=raiz->derecha;
	  		eliminar_nodo(raizDerecha.raiz, dato);	//Se elimina la raiz por la derecha y el dato
	  		raiz->derecha=raizDerecha.raiz;		  	// La raiz queda actualizada.
		  }
	  }

  }
}
// Se crea el nodo que permite obtener el maximo
Nodo* Arbol::obtener_maximo(Nodo *raiz){
	// Cuando la raiz por la derecha sea vacia
	if(raiz->derecha==NULL){
		return raiz;		// Se retorna la raiz
	}
	else{
		return this->obtener_maximo(raiz->derecha); // Si no se cumple la condicion anterior se retorna el maximo con la raiz por la derecha
	}
}

// Esta funcion retornara el puntero a un valor ingresado
bool Arbol::busqueda_nodo(int dato){
	Nodo *inicio = this->raiz;			//Puntero a la raiz
	while(inicio != NULL){				// Cuando el inicio sea distinta a vacia
		if(inicio->valor == dato){ 		// Cuando el valor sea igual al dato
			return true;				// Se retorna un true
		}
		else{
		if(dato > inicio->valor){			// Cuando el dato sea mayor al valor
			inicio = inicio->derecha;		// El inicio se ira a la derecha
		}else{
			inicio = inicio->izquierda;	// Cuando el dato sea menor se ira a la izquierda
		}
	  }
	}
	return false;
}
// Esta funcion de tipo booleana permite modificar el nodo, y verificar que esten los numeros a modificar
bool Arbol::modificacion_nodo(Nodo* &raiz, int dato, int dato_nuevo){
	// Se definen las variables de la funcion y tambien el tipo de ellas.
	bool resultado;
	bool resultado2;
	bool operacion;
	operacion = false;
	resultado = this->busqueda_nodo(dato);
	if(resultado == false){					// Si la busqueda del dato es falsa las operaciones no se pueden completar
	cout << endl << "No se puede completar la operacion el Dato no existe" << endl; // SE IMPRIME QUE EL DATO NO EXISTE
	}else{
		resultado2 = this->busqueda_nodo(dato_nuevo);
		if(resultado2 == true){
			cout << endl << "No se puede completar la operacion el Dato a Modificar existe" << endl; //SE IMPRIME QUE EL DATO NO PUEDE SER MODIFICADO
		}else{
			this->eliminar_nodo(raiz, dato); // se elimina y agregan los valores si existe el dato
			this->agregar_valores(raiz, dato_nuevo);
			operacion = true; // la operacion sera verdadera
		}
	}
	return operacion;
}

// FUNCION QUE ORDENA EN PREORDEN EL ARBOL
void Arbol::arbol_preorden(Nodo *raiz){
  if(raiz != NULL){						// La raiz si es distinta a NULL
    cout << raiz->valor << "-";			// El valor se imprime con esta division
    arbol_preorden(raiz->izquierda);    // Se ordena primero con la izquierda
    arbol_preorden(raiz->derecha);		// Se ordena primero con la derecha
  }
}

// FUNCION QUE ORDENA EN INORDEN EL ARBOL
void Arbol::arbol_inorden(Nodo *raiz){
  if(raiz != NULL){						// La raiz si es distinta a NULL
    arbol_inorden(raiz->izquierda); 	// Se imprime primero lo de la izquierda
    cout << raiz->valor << "-"; 		// El valor se imprime con esta division
    arbol_inorden(raiz->derecha); 		// Luego se imprime lo de la derecha
  }
}

// FUNCION QUE ORDENA EN POSTORDEN EL ARBOL
void Arbol::arbol_posorden(Nodo *raiz){
  if(raiz != NULL){						// Cuando la raiz es diferente de NULL
    arbol_posorden(raiz->izquierda); 	// Se imprime primero lo de la izquierda
    arbol_posorden(raiz->derecha);		// Luego se imprime lo de la derecha
    cout << raiz->valor << "-";			// El valor se imprime con esta division
  }
}

// FUNCION QUE SE ENCARGA DE LA IMPRESION DEL ARBOL
void Arbol::impresion_arbol(Nodo *raiz, int dato){
     if(raiz == NULL)  // Si la raiz es igual a vacio se retorna
          return;
     impresion_arbol(raiz->derecha, dato+1); // Se imprime el arbol por la derecha

     for(int i=0; i<dato; i++) // Se genera un ciclo para imprimir los datos
         cout<<"  ";

     cout<< raiz->valor <<endl; // Se imprime el valor

     impresion_arbol(raiz->izquierda, dato+1); // Se imprime el arbol por la izquierda

  }

 // NODO QUE SE ENCARGA DE OBTENER LA RAIZ
Nodo* Arbol::obtener_raiz(){
	return this->raiz;
}