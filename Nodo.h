/* define la estructura del nodo. */
typedef struct _Nodo {
	// Se define la estructura del valor que es entero
    int valor;
    struct _Nodo *izquierda;		// Se define la estructura del nodo izquierdo
	struct _Nodo *derecha;			// Se define la estructura del nodo derecho
	struct _Nodo *Nodo_padre;		// Se define la estructura del nodo padre
} Nodo;	// El nombre del nodo sera nodo

