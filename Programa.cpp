// Se llaman las librerias a implementar
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

using namespace std;
// Se llama al archivo_grafo_grafo que contiene las funciones que crean el programa
#include "Arbol.h"

// Se crea la clase grafo
class Grafo{
	// Se definen los atributos privados
    private:
    // Se define un nuevo nodo vacio
        Nodo *nuevoarbol = NULL;
	// Se definen los atributos publicos
    public:
        // Constructor de la Clase Grafo
        	Grafo(Nodo *raiz){
	        this->nuevoarbol = raiz;
        }

        // ESTA FUNCION SE ENCARGA DE RECORRER EL ARBOL, recorre en árbol en preorden y agrega datos al archivo.
        void recorrer_arbol(Nodo *p, ofstream &archivo_grafo) { 	// ofstream es el tipo de dato correspondiente a los archivo_grafo_grafos de tipo cpp
	        string info_Arbol;
			// Se enlazan los nodos izquierdo y derecho
	        if (p != NULL) {
	        // Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo_grafo.
	        if (p->izquierda != NULL) {
		        archivo_grafo<< p->valor << "->" << p->izquierda->valor << ";" << endl;
	        }

	        else {
				// Se crean los archivo_grafos y como se mostrara la informacion
		        info_Arbol = to_string(p->valor) + "i";
		        info_Arbol = "\"" + info_Arbol + "\"";
		        archivo_grafo << info_Arbol << "[shape=point]" << endl;
		        archivo_grafo << p->valor << "->" << info_Arbol << ";" << endl;
	        }

	        info_Arbol = p->valor;
			// Se establecen las condiciones cuando la derecha es diferente de vacio
	        if (p->derecha != NULL) {
		        archivo_grafo << p->valor << "->" << p->derecha->valor << ";" << endl;
	        }

	        else {
			// Se establecen las condiciones y como se muestra la informacion de los grafos
		        info_Arbol = to_string(p->valor) + "d";
		        info_Arbol = "\"" + info_Arbol + "\"";
		        archivo_grafo << info_Arbol << "[shape=point]" << endl;
		        archivo_grafo << p->valor << "->" << info_Arbol<< ";" << endl;
	        }

	        // Se realizan los llamados tanto por la izquierda y derecha para crear el nodo
	        recorrer_arbol(p->izquierda, archivo_grafo);
	        recorrer_arbol(p->derecha, archivo_grafo);
	        }
	        return;
        }
	// ESTA FUNCION GRAFICA.
	 /* crea el archivo .txt de salida y le agrega
 		contenido del árbol para generar el grafo (imagen).*/
        void crear_grafo() {
			// Se llama al tipo y al archivo_grafo
	        ofstream archivo_grafo;
	        archivo_grafo.open("datos.txt"); 						// Se abre/crea el archivo_grafo datos.txt
	        archivo_grafo << "digraph G {" << endl; 				 // Se escribe dentro del archivo_grafo datos.txt "digraph G {
	        archivo_grafo << "node [style=filled fillcolor=yellow];" << endl; // Se establece el estilo del diagrama con colores
	        recorrer_arbol(this->nuevoarbol, archivo_grafo); 		// Se llama a la función recursiva que genera el archivo_grafo_grafo
	        archivo_grafo << "}" << endl;							// Se termina de escribir
	        archivo_grafo.close();								// EL archivo_grafo_grafo se cierra
	        system("dot -Tpng -ografo.png datos.txt &");	// Se genera el grafo en texto
	        system("eog grafo.png &");						// Se muestra la imagen del grafo
        }
};

// Se crea el menu de opciones para que el usuario lo pueda ver
int opciones_menu(){
    int opcion;
    // Se imprimen las opciones
    cout << "\n MENU OPCIONES ARBOL \n" << endl;
	cout << "OPCION 1 - INGRESE NUMEROS" << endl;
	cout << "OPCION 2 - VER" << endl;
    cout << "OPCION 3 - MODIFICAR" << endl;
	cout << "OPCION 4 - ELIMINAR" << endl;
    cout << "OPCION 5 - VER GRAFO" << endl;
	cout << "OPCION 0 - SALIR DEL PROGRAMA" << endl;

	// Se imprime para que el usuario ingrese su opcion y se captura el valor
	cout << "Ingrese su opcion:";
	cin >> opcion;
	return opcion;
}

// Se hace un segundo menu para poder ver el orden que del arbol
int menu_recorrido(){
  int opcion2;
  cout << "\n MENU OPCIONES ORDEN \n" << endl;
  cout << "OPCION 1 - PREORDEN" << endl;
  cout << "OPCION 2 - INORDEN" << endl;
  cout << "OPCION 3 - POSTORDEN" << endl;
  //cout << "OPCION 0 - VOLVER AL MENU ANTERIOR" << endl;
	// Se imprime para que el usuario ingrese su opcion y se captura el valor
  cout << "Ingrese su opcion para visualizar el orden:";
  cin >> opcion2;
  return opcion2;
}

// Se crea el main donde se llevan a cabo la funcionalidad del programa
int main(void){
	// Se llaman a las variables y sus tipos
	int opcion;
	int opcion2;
	Arbol arbol;			// Se llama al arbol y se otorga otro nombre
	int dato;
	int dato_nuevo;
	bool resultado;

	opcion=1;			//Para iniciar el ciclo While
	while (opcion != 0){		// Comienza el ciclo while de las opciones
		opcion = opciones_menu();
		switch (opcion){
		// En el caso 1 se insertan numeros
		case 1:
			cout << " INSERTANDO NUMEROS" << endl;
			cout << " Ingrese datos" << endl;
			cin >> dato;									// Se captura el valor dato
			resultado = arbol.busqueda_nodo(dato);			// SE LLAMA A LA FUNCION QUE HACE EL PROCESO DE BUSQUEDA
			if(resultado == true){							// POR MEDIO DE ESTE IF SE VERIFICA SI EL NUMERO EXISTE
				cout << " EL NUMERO YA EXISTE " << endl; 	// El dato ya estaba
			}
			else{
				arbol.agregar_valores(arbol.raiz, dato);	// Se ingresa el valor agregado no repetido
			}
			//cout << arbol.obtener_raiz()->valor;
			break;
			opcion = opciones_menu();

      // En el caso 2 se muestra el contenido del arbol
		case 2:
			cout << " MOSTRANDO CONTENIDO DEL ARBOL " << endl;
			while (opcion2 != 0){ 							// Se llama al otro menu para ver las opciones
				opcion2 = menu_recorrido();
				switch (opcion2){
					// EN EL PRIMER CASO SE MUESTRA EN PREORDEN
					case 1:
						cout << " MOSTRANDO PREORDEN" << endl;
						arbol.arbol_preorden(arbol.obtener_raiz()); // SE LLAMA A LA FUNCION QUE HACE EL PROCESO DE PREORDEN
						break;
					// EN EL SEGUNDO CASO SE MUESTRA EN INORDEN
					case 2:
						cout << " MOSTRANDO INORDEN" << endl;
						arbol.arbol_inorden(arbol.obtener_raiz()); // SE LLAMA A LA FUNCION QUE HACE EL PROCESO DE INORDEN
						break;
					// EN EL TERCER CASO SE MUESTRA POSTORDEN
					case 3:
						cout << " MOSTRANDO POSTORDEN" << endl;
						arbol.arbol_posorden(arbol.obtener_raiz()); // SE LLAMA A LA FUNCION QUE HACE EL PROCESO DE POSTORDEN
						break;
					}
			// MEDIANTE ESTE CASO EL USUARIO SALE DEL SEGUNDO MENU
			case 0:
				break;
		}
			cout << " SALIENDO DEL MENU \n " << endl;
			break;
			opcion = opciones_menu();

     // En el caso 3 se modifican datos numericos
		case 3:
			cout << " MODIFICANDO VALORES " << endl;		// Se imprime que los datos son modificados
			cout << " Ingrese dato Actual: " << endl;		// Se pide el valor actual a modificar
			cin >> dato;
			cout << " Ingrese dato Nuevo: " << endl;		// Se pide el nuevo valor que reemplazara al modificar
			cin >> dato_nuevo;
			resultado = arbol.modificacion_nodo(arbol.raiz, dato, dato_nuevo); // Se llama a la funcion que modifica el nodo
			if(resultado == true){
				cout << endl << "El dato " << dato << " fue cambiado a " << dato_nuevo << endl; // Si las condiciones estan el dato puede ser cambiado
			}else{
				cout << endl << "No se cambio " << dato << endl; // El dato a modificar no ha sido encontrado porque no existe.
			}
            break;
            opcion = opciones_menu();

    // En el caso 4 se elimina el contenido del arbol
		case 4:
			cout << "ELIMINANDO NUMEROS " << endl;
			cout << "Ingrese valor a eliminar " << endl;			// SE PIDE EL INGRESO DEL VALOR A ELIMINAR
			cin >> dato;
			arbol.eliminar_nodo(arbol.raiz, dato);					// SE LLAMA A LA FUNCION QUE HACE EL PROCESO DE ELIMINAR
			cout << " Dato eliminado: " << dato << endl;
			break;
			opcion = opciones_menu();
    // En el caso 5 se muestran los grafos
		case 5:
			cout << " MOSTRANDO CONTENIDO DE LOS GRAFOS " << endl;		// SE IMPRIME QUE SE MUESTRE EL CONTENIDO DE LOS GRAFOS
			Grafo *g = new Grafo(arbol.raiz);							// SE CREA EL NUEVO GRAFO A PARTIR DEL ARBOL Y LA RAIZ
			g-> crear_grafo();										// SE LLAMA A LA FUNCION QUE GRAFICA
			break;
			opcion = opciones_menu();
        }
      }
        system("pause");
      return 0;
 }