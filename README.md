# Guia 4 - Arboles -

# Comenzando --- EXPLICACION PROBLEMATICA----
En el desarrollo de la cuarta guia requiere del lenguaje de programacion C++, La problematica radica en que se realice un programa que cree un arbol binario que contenga solo valores de numeros enteros, se tiene que verificar a su vez que los numero que se ingresen no sean repetidos y que tienen que ser unicos por lo tanto. Se tiene que desarrollar una serie de operaciones basicas  las cuales pueden contener un menu basico para considerar la opcion que desea el usuario hasta que desea finalizar el programa. Se permitira insertar el numero, eliminar un numero buscado, modificar un elemento que se busque (eliminando el valor antiguo y reemplazandolo por el nuevo valor) y tambien mostrar el contenido del arbol en preorden, inorden y post orden. Los elementos que sean ingresados deber ir ordenados.
El desarrollo del trabajo a su vez debe ir acompañado mediante un grafo, el cual tiene que ser visualizado. 

# ---- COMO SE DESARROLLO ----
Para el desarrollo del programa se llevaron a cabo la generacion de cinco archivos, los cuales contienen cada uno los atributos que permiten un optimo desarrollo de la guia. 
- El primero es el archivo Makefile el cual permite que se pueda compilar el codigo mediante la terminal y desarrollar las pruebas de ejecucion en donde se indican los posibles errores del codigo y a la vez indica si la compilacion se ha desarrollado de manera exitosa. 
- _El archivo Nodo.h:_
    -  Este archivo se es el encargado de contener la función que proporciona la estructura del nodo, se define a su vez el entero valor el cual recibirá el número que se ingrese como dato por parte del usuario, tambien estaran las estructuras del nodo las cuales van a la izquierda y derecha, también contiene al nodo padre el cual se encarga de la herencia entre los nodos. Finalmente el nombre del nodo será Nodo. 

- _El archivo Arbol.h_:
    - Este archivo contiene la estructura del nodo para que el código pueda funcionar y se genere esta estructura se genera otra clase llama Árbol, esta contiene atributos públicos y privados los cuales son requeridos para el funcionamiento y detalle del programa, el nodo raíz estará dentro de los atributos privados y tendrá la característica de estar vacío. En los atributos públicos se define el constructor, también se llaman a ciertos nodos que permiten agregar otros, obtener los máximos y la raíz de este. También se generan las funciones principales las cuales permiten agregar los valores al nodo, poder eliminar, llevar a cabo la búsqueda, modificar los nodos (estas dos últimas funciones tienen las características de ser booleanas, lo que significa que retornan un true o un false). Luego se generan las funciones las cuales permiten mostrar cómo será el ordenamiento del árbol y si este será con preorden, inorden o postorden, también se añade la función impresión del árbol la cual permite poder mostrar como el árbol tiene que ser impreso. Finalmente cada detalle de la función y explicación de esta será indicada en su archivo, debido a que este es solo el archivo de cabecera. 

- _El archivo Arbol.cpp:_
   -  En este archivo se llama al archivo cabecera el cual contiene las indicaciones de como deben funcionar las funciones, se llama al constructor de las funciones que en este caso es Arbol. Se crea un nodo el cual permite que se agreguen a una clase arbol los nodos a partir de la izquierda y la derecha(nodos anteriormente creados). Luego se crea la funciones que permiten el funcionamiento del programa, las cuales son: 
   -  Agregar_valores: Permite que se agreguen los valores y al arbol cuando el dato es ingresado por el usuario, tambien para esto se ve la raiz y el estado del nodo al momento de agregar valores, tambien dependiendo del valor del dato y si es mayor o menor al valor este se posicionara a la izquierda o derecha del arbol.
   -  Eliminar_nodo: permite poder analizar si se retorna algun valor de la raiz, tamien permite establecer una serie de condiciones que permiten comparar el valor que el usuario ingresa con los disponibles para eliminar, tambien se verifica que este exista y segun el numero recorrer la raiz para eliminar si pertence al nodo izquierdo o derecha. Para estas condiciones se crearon otros nodos que permiten saber si la raiz de la izquierda y la derecha del nodo se estan modificacndo correctamente. En lineas generales permite eliminar el nodo creado. 
   -  Obtener_maximo: esta funcion Nodo permite obtener el maximo del nodo comparando con el lado derecho y cuando la raiz esta, este nodo es requerido cuando se eliminan para establecer condiciones y verificar las posiciones. 
   -  Busqueda_nodo: Esta funcion de tipo booleano permite poder retornar el puntero con el valor ingresado, cuando el dato a buscar se ingrese por el usuario, se busca el nodo y se recorre el arbol por medio del nodo de la raiz, cuando el valor es igual al dato este estara a la derecha, de modo contrario estara a la izquierda y se retornara si lo encontro al usuario. Esta funcion permite poder validar que los numeros no sean repetidos al ser ingresados. En lineas generales busca si el nodo existe.
   -  Modificacion_nodo: Permite comparar el puntero con la raiz, cuando el valor del inicio sea distinto a vacio, tambien se establecen las condiciones para poder almacenar y cambiar los datos luego ser eliminados para poder modificar los nodos del arbol. Tambien se verifican los cambios por medio de la izquierda y derecha, finalmente al revisar el brazo del arbol se modifican los valores. Permite poder saber si el numero ingresado esta o no para proceder a modificarlo y cambiarlo.
   -  Arbol_preorden: Permite obtener el orden de como son ingresados los datos por el usuario a medida que son ingresados por la terminal, esta funcion es necesario para mostrar el orden de los datos, que se ocupa en el menu principal. Se puede visitar la raiz a su vez recorrer los subarboles izquierdos y derechos. 
  -   Arbol_inorden: Se muestran los datos numericos enteros de manera ordenada al usuario(de manera creciente), para esto se recorre primero el subarbol izquierdo y posteriormente se visita la raiz y al final se recorre el subarbol derecho. 
   -  Arbol_postorden: Se recorre el subarbol izquierdo y se muestra primero las impresiones de este y posteriormente se muestran las del subarbol derecho y de esta manera finalizando se puede visitar la raiz del arbol binario.
   -  Impresion_arbol: Esta funcion se encarga de la impresion del arbol, esta se realiza por la derecha y por la izquierda recorriendo ambos subarboles y posteriormente imprimiendo los valores luego de ser recorridos e impresos. Tambien se imprime el valor que esta en la locacion de la raiz.
   -  Obtener_raiz: Este nodo permite poder buscar y obtener el nodo de la raiz, es necesario a su vez porque la raiz es de caracter privado.

- _El archivo Programa.cpp:_
    - Se incluyen las librerías a implementar y también se llama al archivo que contiene las funciones para que se lleve a cabo el programa(cabecera). En este archivo también se genera la clase Grafo esta tiene sus atributos públicos y privados los cuales permiten que se construya un nodo para poder mostrar el funcionamiento de este en el grafo. Dentro de esta misma clase se crea una función que permite crear el grafo llamando los archivos para que sean abiertos, para que se generen y para también indicar el estilo de este al mostrarlo, luego se obtiene un archivo de texto la cual se transforma mediante system al formato imagen. Posterior a esta función se crea un recorrido la cual permite recorrer por la izquierda y la derecha el nodo, cada vez que se agregue el valor y la dirección este será agregado al nodo mostrando así los nodos padre hijos y hermanos. Esta función es similar y se basó en lo entregado por el profesor Alejandro Valdes. 
Por medio de las funciones recorrer_arbol se puede recorrer la estructura del arbol y pasarlo a un nodo recorrido para luego poder construir el grafo, se establecen a su vez las condiciones por la izquierda y derecha de los nodos con los puntos y nuevos nodos para poder mostrar por condiciones especificas los valores que ingrese el usario. Ademas por la funcion crear_grafo se permite poder abrir el archivo y generar el diagrama tambien permite que se puedan tratar los temas esteticos del diagrama como por ejemplo el color y el estilo. Luego de crear los grafos se crea un archivo txt el luego se convierte a imagen y se muestra al usuario.
    - Se crean dos funciones donde se ejecutan las opciones del menú, a modo general opciones menú(), se encarga de contener la acciones generales que el usuario ejecute en el código, cada opción se guardará y posteriormente en el main se ejecuta la acción. Luego en la función menu_recorrido() se pedirá al usuario que ingrese una opción para visualizar un pequeño menú si para poder visualizar de una manera específica el árbol el cual puede ser preorden, inorden y postorden.
    - Finalmente se crea el menú donde se lleva a cabo la funcionalidad del programa y el árbol, se llaman a las opciones del menú y según la opción que el usuario ingrese se llamará a las funciones en el archivo arbol.cpp las cuales se encargan de poder ingresar números, eliminar, ver el árbol y el orden, modificar los valores, ver el grafo y a su vez salir del programa según sea el caso y la opción que el usuario determine. En la segunda opción donde se puede visualizar de distinta forma según cualquiera de los tres caso se llaman a las funciones anteriormente descritas en el program arbol.cpp las cuales permiten mostrar los datos como estan antes de ser ordenados, cuando estos son ordenados de mayor a menor y cuando estos están en postorden. tambien dentro de las opciones se puede visualizar el grafo y modificar los datos que ingrese el usuario. Finalmente luego de mostrar las opciones el usuario puede abandonar y cerrar el programa. 
LOS DATOS INGRESADOS SON VALIDADOS PARA QUE EXISTAN Y NO SE REPITAN

_Finalmente cabe destacar que el programa funciona con exito resolviendo de esta manera la problematica propuesta por el profesor, ademas las opciones son validadas, el codigo se trato de realizar de manera eficiente, se muestran los grafos y se permite al usuario ingresar las opciones y estas logran un optimo funcionamiento_.

## Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)

# Instalacion

Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando: 
- _lsb_release -a (versión de Ubuntu) _

En donde: Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando. Para descargar un editor de texto como vim se puede descargar de la siguiente manera: 
- _sudo apt install vim _

En donde: Por medio de este editor de texto se puede construir el codigo. En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.

Tambien si se desea ocupar un editor de texto diferente a vim este se puede instalar por la terminal o por el centro de software en donde se escribe el editor que se desea conseguir y luego se ejecuta la descarga, de esta manera se pudo descargar el editor que se ocupa para la realizacion de esta guia el cual es atom, se eligio a la vez este editor debido a que permite una interfaz mejorada con respecto a la compilacion y a su vez permite que se revisen y compilen los archivos de la terminal disminuyendo posibles errores. 

## Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa

### INSTALACION MAKEFILE
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:
- _make_

De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.
- _sudo apt install make_
 
 De esta manera se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.
Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos mediante el comando cd con el nombre de dicha carpeta, luego ver si estan los archivos correctos o vizualizar lo que contiene la carpeta se debe ingresar el comando ls, finalmente se debe escribir make en la terminal.
Si ya se ha ejecutado con anterioridad un make, se tiene que escribir un make clean para borrar los archivos probados con anterioridad. Luego de ejecutar en la terminal el comando:
- _make_

Debe mostrar los nombres de los archivos y el orden de estos con g++ y el nombre del archivo, aqui se puede visualizar si el programa no tiene problematicas. Si hay una problematica sale un mensaje y no salen todos los archivos con g++. Si se desarrollo completamente luego se puede ingresar el siguiente comando:
- _./ con el nombre del programa el cual contiene el main._

De esta manera se observa el funcionamiento del programa y de los demas archivos.

### INSTALACION GRAPHVIZ
Este paquete es un requisito para poder generar y visulizar los grafos creados durante el programa. 

#### GENERACION EN LINEA 
La generacion en linea de los grafos se puede desarrollar de manera online mediante el siguiente sitio:
- http://graphs.grevian.org/graph
En donde se tiene que ingresar la definicion del DOT en el recuadro que se puede visualizar en la pagina y de esta manera se puede visulaizar el grafico resultante para ser descargado. 

#### GENERACION DE MANERA LOCAL 
Para instalarlo de manera local en Ubuntu tanto como en Debian el usuario debe ejecutar el siguiente root:
- _apt-get install graphviz (Debian)_
- _sudo apt-get install graphviz (Ubuntu)_
De esta manera se podra instalar el paquete y comenzar a desarrollar los grafos. 
Posteriormente para poder generr la imagen a partir del archivo fuente se tiene que ingresar en la terminal:
- _dot -Tpng -ografo.png datos.txt_
En donde se generara el archivo con la imagen grafo.png ( grafo simula al nombre del archivo)
Luego para poder visulaizar la imagen se tiene que ejecutar el siguiente comando:
- _eog grafo.png_
De esta forma a partir de un archivo de fuente se puede obtener el archivo que visualice la imagen posterior a la generacion de esta.

# Construido con
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Atom: Editor de código.
- GRAPHVIZ: Herramienta que permite la visualizacion de los grafos.

# Versiones
## Versiones de herramientas:
- Ubuntu 20.04 LTS 
- Atom 1.52.0
- GRAPHVIZ 2.40

Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/guia4-u2

# Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

# Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=730993

- A las Lecturas de internet y videos que me ayudaron a plantear una solucion posible: 
http://c.conclase.net/edd/?cap=006
http://decsai.ugr.es/~jfv/ed1/c++/cdrom4/paginaWeb/grafos.htm
https://blog.martincruz.me/2012/11/arboles-binarios-de-busqueda-c.html
https://www.programacion.com.py/escritorio/c/arboles-en-c
- Videos de ayuda: 
https://www.youtube.com/watch?v=fVQejmEx744
