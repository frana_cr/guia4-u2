#include <iostream>
// Se llama al archivo Nodo, que tiene la estructura
#include "Nodo.h"

#ifndef ARBOL_H
#define ARBOL_H

// Se define la clase arbol
class Arbol{
	// Se definen los atributos privados
    private:
	// Se definen los atributos publicos
    public:
        Nodo *raiz = NULL;								// Se crea el nodo de la raiz vacio
        Arbol();										// Se crea el constructor
        Nodo* agregar_nodo(int dato);					// Se define el nodo que agregara los otros nodos y el dato
        void agregar_valores(Nodo* &raiz, int dato);	// Se crea la funcion que agrega valores
        void eliminar_nodo(Nodo* &raiz, int dato);		// Se crea la funcion que elimina el nodo
        Nodo* obtener_maximo(Nodo* raiz);				//Necesario para cuando se eliminan nodos
        bool busqueda_nodo(int dato);					// Se crea la funcion que busca el nodo
        bool modificacion_nodo(Nodo* &raiz, int dato_anterior, int dato_nuevo);		// Se crea la funcion que modifica el nodo
        void arbol_preorden(Nodo *arbol);				// Se crea la funcion que ordena el arbol por preorden
        void arbol_inorden(Nodo *arbol);				// Se crea la funcion que ordena el arbol por inorden
        void arbol_posorden(Nodo *arbol);				// Se crea la funcion que ordena el arbol por postorden
        void impresion_arbol(Nodo *arbol, int dato);	// Se crea la funcion que imprime el arbol
        Nodo* obtener_raiz();							//Necesario ya que la Raiz es PRIVADA
};
#endif
